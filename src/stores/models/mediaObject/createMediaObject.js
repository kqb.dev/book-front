import {defineStore} from "pinia";
import axios from "@/plugins/axios";

export const useCreateMediaObject = defineStore('createMediaObject', {
    actions: {
        createImage(data) {
            return new Promise((resolve, reject) => {
                axios.post('media_objects', data)
                    .then((res) => {
                        this.createdFile = res.data

                        console.log('Rasm yuklandi')
                        resolve()
                    })
                    .catch(() => {

                        console.warn("Rasm yuklashda xatolik!!!")

                        reject()
                    })

            })
        }
    },

    state() {
        return {
            createdFile: {}

        }
    },
    getters: {
        getCreateFile() {
            return this.createdFile
        }
    }

})