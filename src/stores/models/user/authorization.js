import {defineStore} from "pinia";
import axios from "axios";

export const useAutharization = defineStore('authorization', {
    actions: {
        authUser(data) {
            return new Promise((resolve, reject) => {
                axios.post('http://localhost:9090/api/users/auth', data)
                    .then((response) => {
                        console.log(response)
                        console.log("Tokin olindi")

                        localStorage.setItem('accessToken', response.data.accessToken)
                        localStorage.setItem('refreshToken', response.data.refreshToken)

                        resolve()
                    })
                    .catch(() => {

                        console.warn( "Token olishda xatolik!!!")

                        reject()
                    })

            })
        }
    }

})