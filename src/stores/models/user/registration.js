import {defineStore} from "pinia";
import axios from "axios";
import data from "bootstrap/js/src/dom/data";

export const useRegistration = defineStore('registration', {
    actions: {
        createUser(data) {
            return new Promise((resolve, reject) => {
                axios.post('http://localhost:9090/api/users', data)
                    .then(() => {
                        console.log("then - so'rov muvoffaqqiyatli bo'ganida ishga tushadi")
                        console.log("User yaratildi")

                        resolve()
                    })
                    .catch(() => {
                        console.warn("catch - so'rovda xatolik bo'lganida ishga tushadi.")
                        console.warn( "User yaratishda xatolik!!!")

                        reject()
                    })
                    .finally(() => {
                        console.info("Finally dolm ishlaydi!!")
                    })
            })
        }
    }

})