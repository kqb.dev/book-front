import {defineStore} from "pinia";
import axios from "@/plugins/axios";

export const useFetchCategorys = defineStore('fetchCategorys', {
    actions: {
        fetchCategorys() {
            return new Promise((resolve, reject) => {
                axios.get('categories')
                    .then((res) => {
                        console.log("Categoriyalar olindi")
                        console.warn("Tabriklayman siz buni uddaladingiz!!!")

                        this.categories = res.data['hydra:member']
                        this.categoriesTotalCount = res.data['hydra:totalItem']

                        resolve()
                    })
                    .catch(() => {

                        console.warn("Categoriyalarni olishda  xatolik!!!")

                        reject()
                    })

            })
        }
    },

    state() {
        return {
            categories: [],
            categoriesTotalCount: 0
        }
    },
    getters: {
        getCategories() {
            return this.categories
        }
    }

})