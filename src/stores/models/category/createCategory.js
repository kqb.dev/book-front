import {defineStore} from "pinia";
import axios from "@/plugins/axios";

export const useAddCategory = defineStore('AddCategory', {
    actions: {
        createCategory(data) {
            console.log(data)
            return new Promise((resolve, reject) => {
                axios.post('categories', data)
                    .then(() => {
                        console.log("Categoriya yaratildi")

                        resolve()
                    })
                    .catch(() => {

                        console.warn("Categoriyani yaratishda  xatolik!!!", data)

                        reject()
                    })

            })
        }
    }

})