import {defineStore} from "pinia";
import axios from "@/plugins/axios";

export const useChangeCategory = defineStore('changeCategory', {
    actions: {
        putCategory(id, data) {
            return new Promise((resolve, reject) => {
                axios.put('categories/' + id, data)
                    .then(() => {
                        console.log("Categoriya o'zgartirildi")

                        resolve()
                    })
                    .catch(() => {

                        console.warn("Categoriyani o'zgartirishda  xatolik!!!")

                        reject()
                    })

            })
        }
    }

})