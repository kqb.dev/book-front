import {defineStore} from "pinia";
import axios from "@/plugins/axios";

export const useFetchBook = defineStore('fetchBook', {
    actions: {
        fetchBook(id) {
            return new Promise((resolve, reject) => {
                axios.get('books/' + id)
                    .then((res) => {
                        console.log("Kitob olindi")

                        this.book = {
                            iri: res.data['@id'],
                            id: res.data.id,
                            name: res.data.name,
                            description: res.data.description,
                            text: res.data.text,
                            createdAd: res.data.createdAd,
                            category: {
                                id: res.data.category?.id,
                                name: res.data.category?.name
                            },
                            image: {
                                id: res.data.image?.id,
                                contentUri: res.data.image?.contentUri
                            }
                        }

                        resolve()
                    })
                    .catch(() => {

                        console.warn("Kitoblarni olishda  xatolik!!!")

                        reject()
                    })

            })
        }
    },

    state() {
        return {
            book: {}
        }
    },
    getters: {
        getBook() {
            return this.book
        }
    }

    })

