import {defineStore} from "pinia";
import axios from "@/plugins/axios";

export const useFetchBooks = defineStore('fetchBooks', {
    actions: {
        fetchBooks(url) {
            return new Promise((resolve, reject) => {
                axios.get('books' + url)
                    .then((res) => {
                        console.log("Kitoblar olindi")
                        console.warn("Tabriklayman siz buni uddaladingiz!!!")

                        this.books = res.data['hydra:member']
                        this.booksTotalCount = res.data['hydra:totalItem']

                        resolve()
                    })
                    .catch(() => {

                        console.warn("Kitoblarni olishda  xatolik!!!")

                        reject()
                    })

            })
        }
    },

    state() {
        return {
            books: [],
            booksTotalCount: 0
        }
    },
    getters: {
        getBooks() {
            return this.books
        }
    }

    })

