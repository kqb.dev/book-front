import {defineStore} from "pinia";
import axios from "@/plugins/axios";

export const useRemoveBook = defineStore('removeBook', {
    actions: {
        deleteBook(id) {
            return new Promise((resolve, reject) => {
                axios.delete('books/' + id)
                    .then(() => {
                        console.log("Book o'chirildi")

                        resolve()
                    })
                    .catch(() => {

                        console.warn("Book o'chirishda  xatolik!!!")

                        reject()
                    })

            })
        }
    }

})