import {defineStore} from "pinia";
import axios from "@/plugins/axios";

export const useChangeBook = defineStore('changeBook', {
    actions: {
        putBook(id, data) {
            return new Promise((resolve, reject) => {
                axios.put('books/' + id, data)
                    .then(() => {
                        console.log("KItob o'zgartirildi")

                        resolve()
                    })
                    .catch(() => {

                        console.warn("Kitobni o'zgartirishda  xatolik!!!")

                        reject()
                    })

            })
        }
    }

})