import {createI18n} from "vue-i18n";

import uz from './locales/uz.json'
import ru from './locales/ru.json'
import en from './locales/en.json'

export default createI18n({
    legacy: false,              //i18n'dan Composition Api'ni foydalanish imkonini beradi
    locale: 'uz',               // asosiy tilni ko'rsatadi
    fallbackLocale: 'en',       // ikkilamchi til
    messages: {uz, ru, en}      // tarjima uchun kerakli ro'yxat

})